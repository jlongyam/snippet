
Start git repository

`git init`

Configuration

- Set default branch global setting, usually `master`

    `git config --global init.defaultBranch <name>`

- Change 'default branch'

    `git branch -m <name>`

- Add user name global setting

    `git config --global user.name <name>`

- Add user email global setting

    `git config --global user.email <email>`

- Show list all config

    `git config --list`
